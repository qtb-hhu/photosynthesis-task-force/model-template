from modelbase.ode import Model


def constant(k: float) -> float:
    return k


def proportional(k: float, s: float) -> float:
    return k * s


def get_model() -> Model:
    m = Model()
    m.add_compounds(["S", "P"])
    m.add_parameters({"k_in": 1, "k_1": 1, "k_out": 1})
    m.add_reaction_from_args("v0", constant, {"S": 1}, ["k_in"])
    m.add_reaction_from_args("v1", proportional, {"S": -1, "P": 1}, ["k_1", "S"])
    m.add_reaction_from_args("v2", proportional, {"P": -1}, ["k_out", "P"])
    return m
